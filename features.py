import numpy as np
import cv2
import matplotlib.pyplot as plt

def estimate_label(rgb_image): # Standardized RGB image
  return red_green_orange(rgb_image)

def findNonZero(rgb_image):
  rows, cols, _ = rgb_image.shape
  counter = 0

  for row in range(rows):
    for col in range(cols):
      pixel = rgb_image[row, col]
      if sum(pixel) != 0:
        counter = counter + 1

  return counter

def red_green_orange(rgb_image):
  '''Determines the red, green, and orange content in each image using HSV and
  experimentally determined thresholds. Returns a classification based on the
  values.
  '''
  hsv = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2HSV)
  sum_saturation = np.sum(hsv[:,:,1])
  area = 32*32
  avg_saturation = sum_saturation / area

  sat_low = int(avg_saturation * 1.3)
  val_low = 140

  # green mask
  lower_green = np.array([70,sat_low,val_low])
  upper_green = np.array([100,255,255])
  green_mask = cv2.inRange(hsv, lower_green, upper_green)
  green_result = cv2.bitwise_and(rgb_image, rgb_image, mask = green_mask)

  # orange mask
  lower_orange = np.array([10,sat_low,val_low])
  upper_orange = np.array([60,255,255])
  orange_mask = cv2.inRange(hsv, lower_orange, upper_orange)
  orange_result = cv2.bitwise_and(rgb_image, rgb_image, mask = orange_mask)

  # red mask
  lower_red = np.array([150,sat_low,val_low])
  upper_red = np.array([180,255,255])
  red_mask = cv2.inRange(hsv, lower_red, upper_red)
  red_result = cv2.bitwise_and(rgb_image, rgb_image, mask = red_mask)

  sum_green = findNonZero(green_result)
  sum_orange = findNonZero(orange_result)
  sum_red = findNonZero(red_result)

  if sum_red >= sum_orange and sum_red >= sum_green:
    return 'Red Signal' # red signal
  if sum_orange >= sum_green:
    return 'Orange Signal' # orange signal
  return 'Green Signal' # green signal
