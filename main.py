import cv2
import features
import matplotlib.image as mpimg


def standardize(image):
    standard_im = cv2.resize(image, (32,32))
    return standard_im


if __name__ == '__main__':
    IMAGE_PATH = "/home/webwerks/Documents/cv2_env/traffic-light-classifier/traffic_light_images/training/orange/eda0b0ed-3037-4d55-b834-043b20253890.jpg"

    image = mpimg.imread(IMAGE_PATH)
    stdz_image = standardize(image)
    predicted_signal = features.estimate_label(stdz_image)
    print("The image shows: {}".format(predicted_signal))
